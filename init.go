package carpdriver

import (
	wapc "github.com/wapc/wapc-guest-tinygo"
	pb "gitlab.com/carp-suite/plugins/carpdriver/proto"
	"google.golang.org/grpc"
)

func Init(plugin Plugin) {
	service := PluginService{
		Plugin: plugin,
	}

	wapc.RegisterFunctions(wapc.Functions{
		"configure":     service.Configure,
		"request_start": service.RequestStart,
		"request_end":   service.RequestEnd,
	})
}

func InitGrpc(plugin Plugin) *grpc.Server {
	server := &PluginServiceGrpc{Plugin: plugin}
	maxMsgSize := 220 * 1024 * 1024
	s := grpc.NewServer(grpc.MaxMsgSize(maxMsgSize), grpc.MaxRecvMsgSize(maxMsgSize), grpc.MaxSendMsgSize(maxMsgSize))
	pb.RegisterPluginServer(s, server)
	return s
}
