package carpdriver

type Method int

const (
	GET Method = iota + 1
	POST
	PATCH
	PUT
	DELETE
	OPTIONS
)

func (m Method) String() string {
	return [...]string{"get", "post", "patch", "put", "delete", "options"}[m-1]
}

func (m Method) EnumIndex() int {
	return int(m)
}
