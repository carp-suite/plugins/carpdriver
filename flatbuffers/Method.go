// Code generated by the FlatBuffers compiler. DO NOT EDIT.

package flatbuffers

import "strconv"

type Method int16

const (
	Methodget     Method = 1
	Methodpost    Method = 2
	Methodpatch   Method = 3
	Methodput     Method = 4
	Methoddelete  Method = 5
	Methodoptions Method = 6
)

var EnumNamesMethod = map[Method]string{
	Methodget:     "get",
	Methodpost:    "post",
	Methodpatch:   "patch",
	Methodput:     "put",
	Methoddelete:  "delete",
	Methodoptions: "options",
}

var EnumValuesMethod = map[string]Method{
	"get":     Methodget,
	"post":    Methodpost,
	"patch":   Methodpatch,
	"put":     Methodput,
	"delete":  Methoddelete,
	"options": Methodoptions,
}

func (v Method) String() string {
	if s, ok := EnumNamesMethod[v]; ok {
		return s
	}
	return "Method(" + strconv.FormatInt(int64(v), 10) + ")"
}
