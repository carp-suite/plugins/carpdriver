package carpdriver

import (
	"fmt"
	"strings"
	"time"
)

type Cookie struct {
	Domain  string
	Path    string
	Name    string
	Value   string
	Secure  bool
	Expires time.Time
}

func CookiesStringToArray(cookies string) []*Cookie {
	arr := make([]*Cookie, 0)
	for _, cookie := range strings.Split(cookies, ",") {
		co := new(Cookie)

		for _, attr := range strings.Split(cookie, ";") {
			c := strings.Split(strings.TrimSpace(attr), "=")
			if len(c) == 2 {
				if strings.ToLower(c[0]) == "domain" {
					co.Domain = c[1]
					continue
				}

				if strings.ToLower(c[0]) == "path" {
					co.Path = c[1]
					continue
				}

				if strings.ToLower(c[0]) == "expires" {
					co.Expires, _ = time.Parse(time.RFC3339Nano, c[1])
					continue
				}

				co.Name = c[0]
				co.Value = c[1]
				continue

			} else if len(c) == 1 && c[0] == "Secure" {
				co.Secure = true
			}
		}

		arr = append(arr, co)
	}

	return arr
}

func CookiesToHeaderString(cookies []*Cookie) string {
	var f, ch strings.Builder

	setOtherAttributes := func(c *Cookie) {
		if c.Secure {
			fmt.Fprintf(&f, "Secure; ")
		}

		if c.Domain != "" {
			fmt.Fprintf(&f, "Domain=%s; ", c.Domain)
		}

		if c.Path != "" {
			fmt.Fprintf(&f, "Path=%s; ", c.Path)
		}

		if c.Expires != (time.Time{}) {
			fmt.Fprintf(&f, "Expires=%s; ", c.Expires.Format(time.RFC3339Nano))
		}
	}

	for _, c := range cookies {
		fmt.Fprintf(&f, "%s=%s; ", c.Name, c.Value)
		setOtherAttributes(c)
		fmt.Fprintf(&ch, strings.TrimSuffix(f.String(), "; "))
		fmt.Fprintf(&ch, ", ")
		f.Reset()
	}

	return strings.TrimSuffix(ch.String(), ", ")
}
